#### Character Popper

This app makes a set of stats of the occurrence of the given character in a dictionary.

To see how many words start or end with the given character please fill in the input and click Go.
