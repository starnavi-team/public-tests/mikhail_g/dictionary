import React from 'react';
import { Global, css } from '@emotion/react';
import emotionNormalize from 'emotion-normalize';
import theme from './theme';

const GlobalStyles = () => {
  return (
    <Global
      styles={css`
        ${emotionNormalize}
        
        html {
          box-sizing: border-box;
          font-family: ${theme.fontFamily};
        }

        *, *::before, *::after {
          box-sizing: border-box;
        }

        a {
          text-decoration: none;
          color: inherit;
          display: inline-block;
        }

        ul, li {
          margin: 0;
          padding: 0;
          list-style: none;
        }

        h1, h2, h3, h4, h5, h6 {
          margin: 0;
        }

        p {
          margin: 0;
        }
      `}
    />
  );
};

export default GlobalStyles;
