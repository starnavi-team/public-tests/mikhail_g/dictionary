const theme = {
  cBlack: '#333',
  cRed: '#b03060',
  cTeal: '#008080',
  cGray: '#919191',
  cGrayLight: '#b0b0b0',
  cGrayDark: '#5c5c5c',
  cDivider: '#e5e5e5',
  fontFamily: "'Open Sans', sans-serif",
  shadowDepth3: '0 6px 24px rgba(0, 0, 0, 0.12), 0 2px 6px rgba(0, 0, 0, 0.08)'
};

export default theme;
