import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './components/app/app';
import { StoreContext } from './hooks/use-store';
import { stores } from './store/stores';
import GlobalStyles from './styles/global-styles';

const render = () => (
  <StoreContext.Provider value={stores}>
    <GlobalStyles />
    <App />
  </StoreContext.Provider>
);

ReactDOM.render(render(), document.getElementById('root'));
