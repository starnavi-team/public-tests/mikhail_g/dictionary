import React, {
  Fragment,
  useState,
  useEffect,
  ChangeEvent,
} from 'react';
import { toJS } from 'mobx';
import { observer } from 'mobx-react-lite';
import { useStore } from '../../hooks/use-store';
import { BarChart, Bar, Tooltip, XAxis, YAxis } from 'recharts';
import {
  App,
  Container,
  Header,
  Title,
  Lead,
  InputBox,
  InputBody,
  InputBtn,
  OutputBox,
  StyledBlock
} from './main.styles';

export const Main = observer(() => {
  const { taskStore } = useStore();

  const [letter, setLetter] = useState('');
  const [goClicked, setGoClicked] = useState(false);

  useEffect(() => {
    taskStore.load();
  }, [taskStore]);

  const words = toJS(taskStore.words);

  const filterByRegExp = (regexp: RegExp) => words.filter(item => regexp.test(item)).length;

  const startWithLetter = (letter: string) => {
    if (!letter) return;

    return filterByRegExp(new RegExp(`(\\b${letter}\\S*\\b)`, 'i'));
  };

  const occurenceCount = (letter: string) => {
    if (!letter) return;

    const regexp = new RegExp(`[^${letter}]`, 'ig');
    return words.reduce((count, item) => {
      return count + item.replace(regexp, '').length;
    }, 0);
  };

  const endWithLetter = (letter: string) => {
    if (!letter) return;

    return filterByRegExp(new RegExp(`${letter}$`, 'i'));
  };

  const repeatedLetterCount = () => {
    return filterByRegExp(new RegExp(`([a-z])\\1`, 'i'));
  };

  const getChartData = () => [
      {
        name: 'Words start',
        uv: startWithLetter(letter)
      },
      {
        name: 'Words end',
        uv: endWithLetter(letter)
      },
      {
        name: 'Occurred',
        uv: occurenceCount(letter)
      },
      {
        name: 'Duplicate characters',
        uv: repeatedLetterCount()
      }
    ];

  const onLetterChange = (evt: ChangeEvent<HTMLInputElement>) => {
    setLetter(evt.target.value);
    setGoClicked(false);
  };

  const go = (evt: React.SyntheticEvent) => {
    evt.preventDefault();
    if (!letter) return;
    setGoClicked(true);
  };

  return (
    <App>
      <Header>
        <Title>Character Popper</Title>
        <Lead>Please enter a character to look it up in the dictionary</Lead>
      </Header>
      <Container>
        <InputBox>
          <form onSubmit={go}>
            <InputBody
              placeholder="what character to look for?"
              onChange={onLetterChange}
            />
            <InputBtn type="submit" disabled={!letter}>Go</InputBtn>
          </form>
        </InputBox>
        <OutputBox>
          {goClicked && (
            <Fragment>
              <StyledBlock>
                <p>Words start with the character: {startWithLetter(letter)}</p>
                <p>Words end with the character: {endWithLetter(letter)}</p>
                <p>The character occurred: {occurenceCount(letter)}</p>
                <p>Words with duplicate characters: {repeatedLetterCount()}</p>
              </StyledBlock>
              <BarChart width={400} height={400} data={getChartData()}>
                <Bar dataKey="uv" fill="#8884d8" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
              </BarChart>
            </Fragment>
          )}
        </OutputBox>
      </Container>
    </App>
  );
});
