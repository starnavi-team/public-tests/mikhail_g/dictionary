import styled from '@emotion/styled';
import theme from '../../styles/theme';

export const App = styled.div`
  padding: 96px 48px 128px;
`;

export const Container = styled.div`
  display: flex;
  justify-content: center;

  @media (min-width: 1200px) {
    margin: 0 auto;
    width: 1000px;
  }

  @media (max-width: 1200px) {
    flex-direction: column;
    align-items: center;
  }
`;

export const Header = styled.div`
  padding-bottom: 24px;
  text-align: center;
`;

export const Title = styled.h1`
  padding-bottom: 8px;
  line-height: 42px;
  font-size: 34px;
  color: ${theme.cBlack};
`;

export const Lead = styled.p`
  font-size: 16px;
  color: ${theme.cGray};
`;

export const InputBox = styled.div`
  margin-right: 15px;
  margin-bottom: 15px;
  display: flex;
`;

export const StyledBlock = styled.div`
  margin-bottom: 30px;
`;

export const InputBody = styled.input`
  width: 200px;
  margin-right: 5px;
`;

export const InputBtn = styled.button`
  border: none;
  &:hover {
   cursor: pointer;
  }
`;

export const OutputBox = styled.div`
  width: 300px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
