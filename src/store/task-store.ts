import { makeAutoObservable } from 'mobx';
import words from 'an-array-of-english-words';
import { addToDevtools } from "../lib/mobx/add-to-devtools";

export class TaskStore {
  words: string[] = [];

  constructor() {
    makeAutoObservable(this);
    addToDevtools(this);
  }

  load() {
    this.words = words;
  }
}
